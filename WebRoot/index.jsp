<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>员工管理系统</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">

<link rel="stylesheet" href="css/bootstrap.min.css" media="screen">
<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css"
	media="screen">
<link href="css/dashboard.css" rel="stylesheet">

</head>
<body>

	<jsp:include page="/top_menu.jsp"></jsp:include>
	<div class="container-fluid" style="padding-top: 50px">
		<div class="row">
			<jsp:include page="/left_menu.jsp"></jsp:include>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main"">
			<div class="box">
					<div class="panel-heading">
						<i class="fa fa-user"></i> 欢迎页
					</div>
					<div class="panel-body">
					<h1>欢迎登陆楚越员工管理系统</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/ie-emulation-modes-warning.js"
		charset="UTF-8"></script>
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"
		charset="UTF-8"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/bootstrap-datetimepicker.js"
		charset="UTF-8"></script>
	<script type="text/javascript" src="js/bootstrap-datetimepicker.fr.js"
		charset="UTF-8"></script>
	<script type="text/javascript">
    
	$('.form_date').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
	
</script>
	<script type="text/javascript">
    function deletelogs(id)
    {
        if(confirm("确认删除吗？"))
        {
            window.location.href = "/DeleteLogs?id="+id;
        }
    }
</script>
</body>
</html>
