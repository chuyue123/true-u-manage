<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title></title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet" href="css/bootstrap.min.css" media="screen">	
<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" media="screen">
<link href="css/dashboard.css" rel="stylesheet">
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>

<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="css/style-metro.css" rel="stylesheet" type="text/css"/>
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<link href="css/style-responsive.css" rel="stylesheet" type="text/css"/>
<!-- <link href="css/default.css" rel="stylesheet" type="text/css" id="style_color"/>
 --><link href="css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
<link href="css/daterangepicker.css" rel="stylesheet" type="text/css" />
<link href="css/fullcalendar.css" rel="stylesheet" type="text/css"/>
<link href="css/jqvmap.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="css/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
<script type="text/javascript" src="js/ie-emulation-modes-warning.js" charset="UTF-8"></script>
  </head>
  
  <body>

		<div class="col-sm-4 col-md-3 sidebar page-sidebar nav-collapse collapse">       
			<ul class="page-sidebar-menu ">
			<li class="">
				<a href="javascript:;">
					<i class="icon-cogs"></i> 
					<span class="title">技术部</span>
					<span class="arrow"></span>
				</a>
				<ul class="sub-menu">
					<li >
						<a href="layout_horizontal_sidebar_menu.html">技术日程表</a>
					</li>
					<li >
						<a href="layout_horizontal_menu1.html"><i class="fa fa-camera-retro fa-lg"></i> 周计划</a>
					</li>
					<li >
						<a href="layout_horizontal_menu2.html">技术员信息表</a>
					</li>
				</ul>
				</li>
				
				<li class="">
					<a href="javascript:;">
					<i class="icon-bookmark-empty"></i> 
					<span class="title">销售部</span>
					<span class="arrow "></span>
					</a>
				<ul class="sub-menu">
					<li >
						<a href="ui_general.html">销售日程表</a>
					</li>
					<li >
						<a href="ui_buttons.html">周计划</a>
					</li>
					<li >
						<a href="ui_modals.html">销售信息</a>
					</li>
				</ul>
				</li>

				<li class="">
					<a href="javascript:;">
					<i class="icon-table"></i> 
					<span class="title">客户信息</span>
					<span class="arrow "></span>
					</a>
				<ul class="sub-menu">
					<li >
						<a href="form_layout.html">客户信息</a>
					</li>
					<li >
						<a href="form_samples.html">合同信息</a>
					</li>
					<li >
						<a href="form_component.html">
						<i class=""></i>回款信息</a>

					</li>
				</ul>
				</li>

				<li class="">
					<a href="javascript:;">
					<i class="icon-briefcase"></i> 
					<span class="title">管理员</span>
					<span class="arrow "></span>
					</a>
				<ul class="sub-menu">
					<li >
						<a href="page_timeline.html">
						<i class="icon-time"></i>信息管理</a>
					</li>
					<li >
						<a href="User_getSelect.action">
						<i class="icon-cogs"></i>管理员信息</a>
					</li>
					<li >
						<a href="User_getAdd.action">
						<i class="icon-comments"></i>添加用户</a>
					</li>
				</ul>
				</li>
			</ul>
        </div>
<script src="js/jquery-1.10.1.min.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="js/jquery.blockui.min.js" type="text/javascript"></script>  
<script src="js/jquery.cookie.min.js" type="text/javascript"></script>
<script src="js/jquery.uniform.min.js" type="text/javascript" ></script>
<script src="js/jquery.vmap.js" type="text/javascript"></script>   
<script src="js/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="js/jquery.vmap.world.js" type="text/javascript"></script>
<script src="js/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="js/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="js/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="js/jquery.vmap.sampledata.js" type="text/javascript"></script>  
<script src="js/jquery.flot.js" type="text/javascript"></script>
<script src="js/jquery.flot.resize.js" type="text/javascript"></script>
<script src="js/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="js/date.js" type="text/javascript"></script>
<script src="js/daterangepicker.js" type="text/javascript"></script>     
<script src="js/jquery.gritter.js" type="text/javascript"></script>
<script src="js/fullcalendar.min.js" type="text/javascript"></script>
<script src="js/jquery.easy-pie-chart.js" type="text/javascript"></script>
<script src="js/jquery.sparkline.min.js" type="text/javascript"></script>  
<script src="js/app.js" type="text/javascript"></script>
<script src="js/index.js" type="text/javascript"></script>        

<script>

		jQuery(document).ready(function() {    

		   App.init(); // initlayout and core plugins

		   Index.init();

		   Index.initJQVMAP(); // init index page's custom scripts

		   Index.initCalendar(); // init index page's custom scripts

		   Index.initCharts(); // init index page's custom scripts

		   Index.initChat();

		   Index.initMiniCharts();

		   Index.initDashboardDaterange();

		   Index.initIntro();

		});

</script>
  </body>
</html>