<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>修改用户</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">

<link rel="stylesheet" href="../../css/bootstrap.min.css" media="screen">
<link rel="stylesheet" href="../../css/bootstrap-datetimepicker.min.css"
	media="screen">
<link href="../../css/dashboard.css" rel="stylesheet">
<script type="text/javascript">
function error(){
	var msg="";
	msg="<%=request.getAttribute("msg")%>";
		if (msg.length > 0) {
			alert(msg);
		}
	}
</script>	
</head>
<body onload="error()">

	<jsp:include page="/top_menu.jsp"></jsp:include>
	<div class="container-fluid" style="padding-top: 50px">
		<div class="row">
			<jsp:include page="/left_menu.jsp"></jsp:include>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main"">
			<div class="box">
					<div class="panel-heading"><i class="fa fa-user"></i> 修改用户</div>
					<div class="bar pull-right"><a href="javascript:history.go(-1);">返回</a></div>
					<div class="panel-body">
					
		<form id="query_form" class="form-horizontal form-well" role="form" action="User_postUpdate.action?id=${user.id }" method="post">
            <div class="form-group">
                <label for="name" class="col-md-4 control-label">用户名</label>
                <div class="col-md-3">
                    <input type="text" id="name" name="name" class="form-control" value="${user.name}" placeholder="6到64位" readonly="readonly" />
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-md-4 control-label">密码</label>
                <div class="col-md-3">
                    <input type="password" id="password" name="password" class="form-control" value="${user.password}" placeholder="6到64位">
                </div>
            </div>
            <div class="form-group">
              <label for="email" class="col-md-4 control-label">Email</label>
                <div class="col-md-3">
                    <input type="text" id="email" name="email" class="form-control" value="${user.email}" placeholder="6到64位">
                </div>
            </div>
            <div class="form-group">
                <label for="tel" class="col-md-4 control-label">电话号码</label>
                <div class="col-md-3">
                    <input type="text" id="tel" name="tel" placeholder="6到64位"  value="${user.tel}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                 <label for="role" class="col-md-4 control-label">角色</label>
                 <div class="col-md-3">
                     <select id="role" name="role" class="form-control">
                        <option value="1">${map['1'] }</option>
                        <option value="2">${map['2'] }</option>
                        <option value="3">${map['3'] }</option>
                        <option value="0">${map['0'] }</option>
                    </select>
				</div>
            </div>
            <div class="form-group">
                <label for="photo" class="col-md-4 control-label">相片</label>
                <div class="col-md-3">
                    <input type="text" id="photo" name="photo" placeholder="6到64位"  value="${user.photo}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                 <label for="status" class="col-md-4 control-label">选择状态</label>
                 <div class="col-md-3">
                     <select id="status" name="status" class="form-control">
                        <option value="0">在职</option>
                            <option value="1">离职</option>
                    </select>
				</div>
            </div>
            <div class="form-group">
                 <label for="sex" class="col-md-4 control-label">性别</label>
                 <div class="col-md-3">
                     <select id="sex" name="sex" class="form-control">
                        <option value="男">男</option>
                            <option value="女">女</option>
                    </select>
				</div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-5 col-md-7">
                    <button type="submit" class="btn btn-primary">更新</button>
                </div>
            </div>
        </form>
					
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="../../js/ie-emulation-modes-warning.js"
		charset="UTF-8"></script>
	<script type="text/javascript" src="../../js/jquery-1.9.1.min.js"
		charset="UTF-8"></script>
	<script type="text/javascript" src="../../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../../js/bootstrap-datetimepicker.js"
		charset="UTF-8"></script>
	<script type="text/javascript" src="../../js/bootstrap-datetimepicker.fr.js"
		charset="UTF-8"></script>
</body>
</html>
