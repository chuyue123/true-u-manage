<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>周计划</title>

<link rel="stylesheet" href="../../css/bootstrap.min.css" media="screen">
<link rel="stylesheet" href="../../css/bootstrap-datetimepicker.min.css" media="screen">
<link href="../../css/dashboard.css" rel="stylesheet">
</head>
<body>

	<jsp:include page="/top_menu.jsp"></jsp:include>
	<div class="container-fluid" style="padding-top: 50px">
		<div class="row">
			<jsp:include page="/left_menu.jsp"></jsp:include>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main"">
			<div class="box">
			<div class="panel-heading"><i class="fa fa-user"></i> 查询周计划</div>
	<div class="panel-body">
		<form id="query_form" class="form-horizontal form-well" role="form" action="User_postSelect.action" method="post">
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">用户名</label>
                <div class="col-md-3">
                    <input type="text" id="name" name="name" class="form-control" value="${user.name}" placeholder="模糊查询">
                </div>
                           
                <label for="email" class="col-md-2 control-label">Email</label>
                <div class="col-md-3">
                    <input type="text" id="email" name="email" class="form-control" value="${user.email}" placeholder="精确查询">
                </div>
            </div>
              
            <div class="form-group">
                <label for="tel" class="col-md-2 control-label">电话号码</label>
                <div class="col-md-3">
                    <input type="text" id="tel" name="tel" placeholder="精确查询"  value="${user.tel}" class="form-control">
                </div>
                 <label for="status" class="col-md-2 control-label">选择状态</label>
                 <div class="col-md-3">
                     <select id="status" name="status" class="form-control">
                        <option></option>
                            <option value="${user.status}"}>${user.status}</option>
                    </select>
				</div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-5 col-md-7">
                    <button type="submit" class="btn btn-primary">查询</button>
                </div>
            </div>
        </form>
        
        <table class="table table-hover">
            <thead>
            <tr>
                <th>用户姓名</th>
                <th>性别</th>
                <th>Email</th>
                <th>电话号码</th>
                <th>角色</th>
                <th>状态</th>
                <th>相片</th>
                <th>操作<a></a></th>
            </tr>
            </thead>
		<c:forEach items="${listUsers }" var="ul">
            <tbody>
            <tr>
                <td>${ul.name}</td>
                <td>${ul.sex}</td>
                <td>${ul.email}</td>
                <td>${ul.tel}</td>
                <td>${map[ul.role]}</td>
                <td>${ul.status}</td>
                <td>${ul.photo}</td>
                <td>
                <c:if test="${ul.role != 0}">
                <a class="opt-btn btn-default" href="User_getUpdate.action?id=${ul.id}">修改</a>
                <a class="opt-btn btn-default" href="javascript:deletelogs('${ul.id}')">删除</a>
                </c:if>
                </td>
            </tr>
            </tbody>
			</c:forEach>
        </table>
	</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="../../js/ie-emulation-modes-warning.js"
		charset="UTF-8"></script>
	<script type="text/javascript" src="../../js/jquery-1.9.1.min.js"
		charset="UTF-8"></script>
	<script type="text/javascript" src="../../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../../js/bootstrap-datetimepicker.js"
		charset="UTF-8"></script>
	<script type="text/javascript" src="../../js/bootstrap-datetimepicker.fr.js"
		charset="UTF-8"></script>
<script type="text/javascript">
    function deletelogs(id)
    {
        if(confirm("确认删除吗？"))
        {
            window.location.href = "User_getDelete.action?id="+id;
        }
    }
</script>
</body>
</html>
