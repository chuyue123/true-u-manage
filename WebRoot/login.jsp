<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<title>登陆</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link rel="stylesheet" href="css/bootstrap.min.css" media="screen">
<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" media="screen">
<link href="css/dashboard.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="js/ie-emulation-modes-warning.js" charset="UTF-8"></script>
<script type="text/javascript">
function error(){
	var msg="";
	msg="<%=request.getAttribute("msg")%>"
		if (msg.length > 4) {
			alert(msg);
		}
	}

	function validate() {
		if (document.login.name.value == "") {
			alert("用户名不能为空.");
			document.login.name.focus();
			return false;
		}
		if (document.login.password.value == "") {
			alert("密码不能为空.");
			document.login.password.focus();
			return false;
		}
		return true;
	}
</script>
</head>

<body onload="error()">
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	</nav>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 col-md-offset-4 " style="padding-top: 150px">
				<h3 class="col-sm-offset-4 col-sm-8">员工管理系统</h3>
				<form onsubmit="return validate()" class="form-horizontal form-well" role="form" name="login"
					method="post" action="User_login.action"
					style="background-image: url('image/login.png');">
					<div class="form-group">
						<label for="name" class="col-sm-3 control-label">用户名</label>
						<div class="col-sm-8 input-group">
							<input type="text" class="form-control" name="name" id="name"
								placeholder="name"> <span class="input-group-addon"><span
								class="glyphicon glyphicon-user form-control-feedback"></span> </span>
						</div>
					</div>
					<div class="form-group">
						<br> <label for="password" class="col-sm-3 control-label">密&nbsp;&nbsp;码</label>
						<div class="col-sm-8 input-group">
							<input type="password" class="form-control" name="password"
								id="password" placeholder="Password"> <span
								class="input-group-addon"><span
								class="glyphicon glyphicon-lock form-control-feedback"></span> </span>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-5 col-sm-7">
							<button type="submit" class="btn btn-default">登陆</button>
						</div>
					</div>
				</form>
			</div>
</body>
</html>
