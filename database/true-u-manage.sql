/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50539
Source Host           : localhost:3306
Source Database       : true-u-manage

Target Server Type    : MYSQL
Target Server Version : 50539
File Encoding         : 65001

Date: 2016-05-05 15:08:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_contract
-- ----------------------------
DROP TABLE IF EXISTS `t_contract`;
CREATE TABLE `t_contract` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `Number` int(11) DEFAULT NULL COMMENT '合同编号',
  `Points` int(11) DEFAULT NULL COMMENT '点数',
  `Monney` double DEFAULT NULL COMMENT '金额',
  `Contractlinkprodcutid` int(11) DEFAULT NULL COMMENT '产品与合同链接id',
  `Service` varchar(4000) DEFAULT NULL COMMENT '服务',
  `Manday` int(11) DEFAULT NULL COMMENT '人天',
  `Projectnumber` int(11) DEFAULT NULL COMMENT '项目编号',
  `Customerid` int(11) DEFAULT NULL COMMENT '客户id',
  PRIMARY KEY (`Id`),
  KEY `FK_t_contract_Customerid` (`Customerid`),
  CONSTRAINT `FK_t_contract_Customerid` FOREIGN KEY (`Customerid`) REFERENCES `t_customer` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='合同表';

-- ----------------------------
-- Records of t_contract
-- ----------------------------

-- ----------------------------
-- Table structure for t_cost
-- ----------------------------
DROP TABLE IF EXISTS `t_cost`;
CREATE TABLE `t_cost` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `Worktype` int(11) DEFAULT NULL COMMENT '类型(技术或销售)',
  `Receipts` varchar(4000) DEFAULT NULL COMMENT '小票',
  `Invoice` varchar(4000) DEFAULT NULL COMMENT '发票',
  `Type` varchar(4000) DEFAULT NULL COMMENT '类型（油费,饭费等）',
  `Sum` double DEFAULT NULL COMMENT '金额',
  `Workerid` int(11) DEFAULT NULL COMMENT '申请者id',
  `Trip` int(11) DEFAULT NULL COMMENT '行程',
  `Time` datetime DEFAULT NULL COMMENT '日期',
  `Customerid` int(11) DEFAULT NULL COMMENT '客户id',
  `Projectnumber` int(11) DEFAULT NULL COMMENT '项目编号',
  `Customercontractid` int(11) DEFAULT NULL COMMENT '客户联系方式id',
  `Remark` varchar(4000) DEFAULT NULL COMMENT '备注',
  `Destination` varchar(4000) DEFAULT NULL COMMENT '目的地',
  `Strartplace` varchar(4000) DEFAULT NULL COMMENT '始发地',
  PRIMARY KEY (`Id`),
  KEY `FK_t_cost_Workerid` (`Workerid`),
  KEY `FK_t_cost_Customercontractid` (`Customercontractid`),
  CONSTRAINT `FK_t_cost_Customercontractid` FOREIGN KEY (`Customercontractid`) REFERENCES `t_customer_contact` (`Id`),
  CONSTRAINT `FK_t_cost_Workerid` FOREIGN KEY (`Workerid`) REFERENCES `t_user` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='花费表';

-- ----------------------------
-- Records of t_cost
-- ----------------------------

-- ----------------------------
-- Table structure for t_customer
-- ----------------------------
DROP TABLE IF EXISTS `t_customer`;
CREATE TABLE `t_customer` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `Name` varchar(4000) DEFAULT NULL COMMENT '客户名称',
  `Industry` varchar(4000) DEFAULT NULL COMMENT '行业 下拉选择 参考CRM系统',
  `Product` varchar(4000) DEFAULT NULL COMMENT '产品 下拉选择 参考CRM系统',
  `Saler` int(11) DEFAULT NULL COMMENT '销售代表',
  `Sproduct` varchar(4000) DEFAULT NULL COMMENT '报备产品 下拉选择 参考CRM系统 多选',
  `Sstatus` int(11) DEFAULT NULL COMMENT '报备状态',
  `Designernumber` varchar(4000) DEFAULT NULL COMMENT '设计人员人数',
  `Visitnumber` int(11) DEFAULT NULL COMMENT '拜访次数 根据销售日程等数据修改次此列',
  `ERP` varchar(4000) DEFAULT NULL COMMENT '所使用ERP 下拉选择 参考CRM系统',
  `PDM` varchar(4000) DEFAULT NULL COMMENT '所使用PDM 下拉选择 参考CRM系统',
  `Createtime` datetime DEFAULT NULL COMMENT '创建日期 数据创建时间',
  `Number` int(11) DEFAULT NULL COMMENT 'CRM系统中对应编号 CRM中编号 选填',
  `Visited` int(11) DEFAULT NULL COMMENT '是否拜访 是否拜访 1是 2否',
  `Address` varchar(4000) DEFAULT NULL COMMENT '地址 详细地址 精确到园区',
  `Description` varchar(4000) DEFAULT NULL COMMENT '客户说明',
  `Listed` int(11) DEFAULT NULL COMMENT '是否上市公司 是1 否2',
  `Level` int(11) DEFAULT NULL COMMENT '级别 级别当 级别表更新时也需同步更新',
  `Maindrawsoftware` varchar(4000) DEFAULT NULL COMMENT '主要三维软件 下拉选择 参考CRM系统 多选',
  PRIMARY KEY (`Id`),
  KEY `FK_t_customer_Saler` (`Saler`),
  CONSTRAINT `FK_t_customer_Saler` FOREIGN KEY (`Saler`) REFERENCES `t_user` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='客户表';

-- ----------------------------
-- Records of t_customer
-- ----------------------------

-- ----------------------------
-- Table structure for t_customer_contact
-- ----------------------------
DROP TABLE IF EXISTS `t_customer_contact`;
CREATE TABLE `t_customer_contact` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `Name` varchar(4000) DEFAULT NULL COMMENT '客户姓名',
  `Department` varchar(4000) DEFAULT NULL COMMENT '部门',
  `Position` varchar(4000) DEFAULT NULL COMMENT '职务',
  `QQ` int(11) DEFAULT NULL COMMENT 'QQ号码',
  `Photo` varchar(4000) DEFAULT NULL COMMENT '相片',
  `Tel` varchar(4000) DEFAULT NULL COMMENT '电话号码',
  `Email` varchar(4000) DEFAULT NULL COMMENT '邮箱',
  `Status` int(11) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='客户联系方式表';

-- ----------------------------
-- Records of t_customer_contact
-- ----------------------------

-- ----------------------------
-- Table structure for t_customer_level
-- ----------------------------
DROP TABLE IF EXISTS `t_customer_level`;
CREATE TABLE `t_customer_level` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `Customerid` int(11) DEFAULT NULL COMMENT '客户id',
  `Newlevel` int(11) DEFAULT NULL COMMENT '新级别',
  `Oldlevel` int(11) DEFAULT NULL COMMENT '原先级别',
  `Reason` varchar(4000) DEFAULT NULL COMMENT '变更原因',
  PRIMARY KEY (`Id`),
  KEY `FK_t_customevel_Customerid` (`Customerid`),
  CONSTRAINT `FK_t_customevel_Customerid` FOREIGN KEY (`Customerid`) REFERENCES `t_customer` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='客户级别表';

-- ----------------------------
-- Records of t_customer_level
-- ----------------------------

-- ----------------------------
-- Table structure for t_message
-- ----------------------------
DROP TABLE IF EXISTS `t_message`;
CREATE TABLE `t_message` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `Description` varchar(4000) DEFAULT NULL COMMENT '信息描述',
  `Type` varchar(4000) DEFAULT NULL COMMENT '信息类型 信息类型判断以后怎么处理 审核周报类型 ',
  `Createtime` datetime DEFAULT NULL COMMENT '信息时间',
  `Status` int(11) DEFAULT NULL COMMENT '信息状态 是否已读',
  `Title` varchar(4000) DEFAULT NULL COMMENT '标题',
  `Recipientid` int(11) DEFAULT NULL COMMENT '接受者id',
  `Sender` int(11) DEFAULT NULL COMMENT '发信人id',
  PRIMARY KEY (`Id`),
  KEY `FK_t_message_Recipientid` (`Recipientid`),
  KEY `FK_t_message_Sender` (`Sender`),
  CONSTRAINT `FK_t_message_Sender` FOREIGN KEY (`Sender`) REFERENCES `t_user` (`Id`),
  CONSTRAINT `FK_t_message_Recipientid` FOREIGN KEY (`Recipientid`) REFERENCES `t_user` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='信息表';

-- ----------------------------
-- Records of t_message
-- ----------------------------

-- ----------------------------
-- Table structure for t_returnmoney
-- ----------------------------
DROP TABLE IF EXISTS `t_returnmoney`;
CREATE TABLE `t_returnmoney` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `Money` double DEFAULT NULL COMMENT '金额',
  `Invoice` varchar(4000) DEFAULT NULL COMMENT '发票',
  `Invoicenumber` int(11) DEFAULT NULL COMMENT '发票编号',
  `Contractid` int(11) DEFAULT NULL COMMENT '合同id',
  PRIMARY KEY (`Id`),
  KEY `FK_t_returnmoney_Contractid` (`Contractid`),
  CONSTRAINT `FK_t_returnmoney_Contractid` FOREIGN KEY (`Contractid`) REFERENCES `t_contract` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='回款';

-- ----------------------------
-- Records of t_returnmoney
-- ----------------------------

-- ----------------------------
-- Table structure for t_s_schedule
-- ----------------------------
DROP TABLE IF EXISTS `t_s_schedule`;
CREATE TABLE `t_s_schedule` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `Workcontent` varchar(4000) DEFAULT NULL COMMENT '工作内容',
  `Cost` double DEFAULT NULL COMMENT '花费',
  `Remark` varchar(4000) DEFAULT NULL COMMENT '备注 备注特殊情况',
  `Time` datetime DEFAULT NULL COMMENT '日期',
  `Workerid` int(11) DEFAULT NULL COMMENT '申请者id 默认当天 可选择',
  `Customerid` int(11) DEFAULT NULL COMMENT '客户id 从创建的客户中选择 如没有提供创建客户的界面后 选择',
  `Worktime` int(11) DEFAULT NULL COMMENT '工时 几个小时的工时',
  `Signin` varchar(4000) DEFAULT NULL COMMENT '签到 百度地图定位到位置的名称 不可手填',
  `Worktype` varchar(4000) DEFAULT NULL COMMENT '工作类型 拜访客户 电话沟通 培训 开会 请假等 技术和销售有区别',
  PRIMARY KEY (`Id`),
  KEY `FK_t_s_schedule_Workerid` (`Workerid`),
  KEY `FK_t_s_schedule_Customerid` (`Customerid`),
  CONSTRAINT `FK_t_s_schedule_Customerid` FOREIGN KEY (`Customerid`) REFERENCES `t_customer` (`Id`),
  CONSTRAINT `FK_t_s_schedule_Workerid` FOREIGN KEY (`Workerid`) REFERENCES `t_user` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='销售日程表';

-- ----------------------------
-- Records of t_s_schedule
-- ----------------------------

-- ----------------------------
-- Table structure for t_t_schedule
-- ----------------------------
DROP TABLE IF EXISTS `t_t_schedule`;
CREATE TABLE `t_t_schedule` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `Projectnumber` int(11) DEFAULT NULL COMMENT '项目编号',
  `Workcontent` varchar(4000) DEFAULT NULL COMMENT '工作内容',
  `Remark` varchar(4000) DEFAULT NULL COMMENT '备注',
  `Time` datetime DEFAULT NULL COMMENT '日期',
  `Worktime` int(11) DEFAULT NULL COMMENT '工时',
  `Workerid` int(11) DEFAULT NULL COMMENT '申请者id',
  `Worktype` varchar(4000) DEFAULT NULL COMMENT '工作类型',
  PRIMARY KEY (`Id`),
  KEY `FK_t_t_schedule_Workerid` (`Workerid`),
  CONSTRAINT `FK_t_t_schedule_Workerid` FOREIGN KEY (`Workerid`) REFERENCES `t_user` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='技术日程表';

-- ----------------------------
-- Records of t_t_schedule
-- ----------------------------

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `Name` varchar(4000) NOT NULL COMMENT '用户名 姓名注册',
  `Sex` varchar(4000) DEFAULT NULL COMMENT '性别 后期编辑',
  `Email` varchar(4000) DEFAULT NULL COMMENT '企业 后期编辑 企业邮箱',
  `Tel` varchar(4000) DEFAULT NULL COMMENT '手机号码 后期编辑 手机号码',
  `Role` varchar(4000) DEFAULT NULL COMMENT '角色 销售或技术',
  `Status` int(11) DEFAULT NULL COMMENT '状态 是否离职等 1和2',
  `Photo` varchar(4000) DEFAULT NULL COMMENT '相片 上传到电子仓库形式',
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='用户表';

-- ----------------------------
-- Records of t_user
-- ----------------------------

-- ----------------------------
-- Table structure for t_week_plan
-- ----------------------------
DROP TABLE IF EXISTS `t_week_plan`;
CREATE TABLE `t_week_plan` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `Description` varchar(4000) DEFAULT NULL COMMENT '周计划描述',
  `Attachment` varchar(4000) DEFAULT NULL COMMENT '附件 上传office文档',
  `Workerid` int(11) DEFAULT NULL COMMENT '申请者id 申请人',
  `Status` int(11) DEFAULT NULL COMMENT '审核状态 0 成功 1审核 2否决',
  `Managerid` int(11) DEFAULT NULL COMMENT '受审人id 从用用户表中选择',
  `Type` varchar(4000) DEFAULT NULL COMMENT '类型 销售或技术类型',
  PRIMARY KEY (`Id`),
  KEY `FK_t_week_plan_Managerid` (`Managerid`),
  CONSTRAINT `FK_t_week_plan_Managerid` FOREIGN KEY (`Managerid`) REFERENCES `t_user` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='周计划表';

-- ----------------------------
-- Records of t_week_plan
-- ----------------------------
