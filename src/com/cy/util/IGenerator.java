package com.cy.util;

import java.util.List;

/**
 * 泛型接口
 * 
 * @author fzp
 * 
 * @param <T>泛型
 */
public interface IGenerator<T> {

	/**
	 * 添加方法
	 * 
	 * @param t
	 *            实体类
	 * @param s
	 *            查询方法
	 * @return
	 */
	public boolean insert(T t, String s);

	/**
	 * 刪除方法
	 * 
	 * @param t
	 *            实体类
	 * @param s
	 *            查询方法
	 * @return
	 */
	public boolean delete(T t, String s);

	/**
	 * 修改方法
	 * 
	 * @param t
	 *            实体类
	 * @param s
	 *            查询方法
	 * @return
	 */
	public boolean update(T t, String s);

	/**
	 * 取值方法
	 * 
	 * @param t
	 *            实体类
	 * @param s
	 *            查询方法
	 * @return
	 */
	public List<T> getObject(T t, String s);
	
}
