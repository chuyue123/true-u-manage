package com.cy.util;

import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import com.cy.entity.User;

public class Util {
	
	/**
	 * 跳转页面
	 * @param url  页面地址
	 * @param msg  信息
	 * @return
	 */
	public String setReturn(String url, String msg){
		ServletActionContext.getRequest().setAttribute("msg", msg);
		return url;
	}
	public String setReturn(String url, List list){
		ServletActionContext.getRequest().setAttribute("list", list);
		return url;
	}
	public String setReturn(String url, String msg, Map map){
		ServletActionContext.getRequest().setAttribute("map", map);
		ServletActionContext.getRequest().setAttribute("msg", msg);
		return url;
	}
	
}
