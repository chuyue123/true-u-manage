package com.cy.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * 泛型实现接口
 * @author fzp
 *
 * @param <T>
 */
public class GeneratorDaoImpl<T> extends BaseDao implements IGenerator<T> {

/**
 * 	分页获取所有
 * @param pageBean
 * @param s
 * @return
 */
	public List<T> showMsgInfo(PageBean pageBean,String s) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("curPage",
				(pageBean.getCurPage() - 1) * pageBean.getRowsPrePage());
		map.put("rowsPrePage", pageBean.getRowsPrePage());
		return this.getSqlSession().selectList(s, map);
	}

/**
 * 动态查询
 * 	模糊分页查询   统计个数
 * @param parmname1 第一个查询条件
 * @param parmname2 第二个查询条件
 * @param s
 * @return
 */
	public int queryMsgCount(String parmname1, String parmname2,String s) {
		// TODO Auto-generated method stub
		Map<String, Object> pram = new HashMap<String, Object>();
		pram.put("parmname1", "%"+parmname1+"%");
		pram.put("parmname2", "%"+parmname2+"%");
		return this.getSqlSession().selectOne("s", pram);
	}
	/**
	 * 动态查询
	 * 模糊分页查询 
	 * @param curPage
	 * @param rowsPrePage
	 * @param parmname1
	 * @param parmname2
	 * @param s
	 * @return
	 */
	public List<T> like(int curPage, int rowsPrePage, String parmname1,
			String parmname2,String s) {
		// TODO Auto-generated method stub
		Map<String, Object> pramrs = new HashMap<String, Object>();
		pramrs.put("pageNum", curPage);
		pramrs.put("pageSize", rowsPrePage);
		pramrs.put("parmname1", "%"+parmname1+"%");
		pramrs.put("parmname2", "%"+parmname2+"%");
		return this.getSqlSession().selectList("s", pramrs);
	}	
	
	@Override
	public boolean insert(T t,String s) {
		// TODO Auto-generated method stub
		if (this.getSqlSession().insert(s, t) > 0) {
			this.getSqlSession().commit();
			flag = true;
		}
		return flag;
	}

	@Override
	public boolean delete(T t,String s) {
		// TODO Auto-generated method stub
		if(this.getSqlSession().delete(s,t)>0){
			this.getSqlSession().commit();
			flag=true;
		}
		return flag;
	}

	@Override
	public boolean update(T t,String s) {
		// TODO Auto-generated method stub
		if(this.getSqlSession().update(s, t)>0){
			this.getSqlSession().commit();
			flag=true;
		}
		return flag;
	}

	@Override
	public List<T> getObject(T t,String s) {
		// TODO Auto-generated method stub
		return this.getSqlSession().selectList(s, t);
	}

}
