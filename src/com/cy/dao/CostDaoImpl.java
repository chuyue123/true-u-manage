package com.cy.dao;

import java.util.List;

import com.cy.entity.Cost;
import com.cy.util.GeneratorDaoImpl;

/**
 * 花费（需要报销的费用）dao实现
 * 
 * @author fzp
 * 
 */
public class CostDaoImpl extends GeneratorDaoImpl<Cost> {

	@Override
	public boolean insert(Cost cost, String s) {
		// TODO Auto-generated method stub
		return super.insert(cost, s);
	}

	@Override
	public boolean delete(Cost cost, String s) {
		// TODO Auto-generated method stub
		return super.delete(cost, s);
	}

	@Override
	public boolean update(Cost cost, String s) {
		// TODO Auto-generated method stub
		return super.update(cost, s);
	}

	@Override
	public List<Cost> getObject(Cost cost, String s) {
		// TODO Auto-generated method stub
		return super.getObject(cost, s);
	}
}
