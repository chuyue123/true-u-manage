package com.cy.dao;

import java.util.List;
import com.cy.entity.CustomerLevel;
import com.cy.util.GeneratorDaoImpl;

/**
 * 客户级别dao实现
 * 
 * @author fzp
 * 
 */
public class CustomerLevelDaoImpl extends GeneratorDaoImpl<CustomerLevel> {

	@Override
	public boolean insert(CustomerLevel customerLevel, String s) {
		// TODO Auto-generated method stub
		return super.insert(customerLevel, s);
	}

	@Override
	public boolean delete(CustomerLevel customerLevel, String s) {
		// TODO Auto-generated method stub
		return super.delete(customerLevel, s);
	}

	@Override
	public boolean update(CustomerLevel customerLevel, String s) {
		// TODO Auto-generated method stub
		return super.update(customerLevel, s);
	}

	@Override
	public List<CustomerLevel> getObject(CustomerLevel customerLevel, String s) {
		// TODO Auto-generated method stub
		return super.getObject(customerLevel, s);
	}
}
