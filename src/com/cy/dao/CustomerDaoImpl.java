package com.cy.dao;

import java.util.List;

import com.cy.entity.Customer;
import com.cy.util.GeneratorDaoImpl;

public class CustomerDaoImpl  extends GeneratorDaoImpl<Customer>  {

	@Override
	public boolean insert(Customer customer, String s) {
		 
		return super.insert(customer, s);
	}

	@Override
	public boolean delete(Customer customer, String s) {
		 
		return super.delete(customer, s);
	}

	@Override
	public boolean update(Customer customer, String s) {
		 
		return super.update(customer, s);
	}

	@Override
	public List<Customer> getObject(Customer customer, String s) {
		 
		return super.getObject(customer, s);
	}

	
}

