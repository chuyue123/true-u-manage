package com.cy.dao;

import java.util.List;

import com.cy.entity.Returnmoney;
import com.cy.util.GeneratorDaoImpl;

/**
 * 回款dao实现
 * 
 * @author fzp
 * 
 */
public class ReturnmoneyDaoImpl extends GeneratorDaoImpl<Returnmoney> {

	@Override
	public boolean insert(Returnmoney returnmoney, String s) {
		// TODO Auto-generated method stub
		return super.insert(returnmoney, s);
	}

	@Override
	public boolean delete(Returnmoney returnmoney, String s) {
		// TODO Auto-generated method stub
		return super.delete(returnmoney, s);
	}

	@Override
	public boolean update(Returnmoney returnmoney, String s) {
		// TODO Auto-generated method stub
		return super.update(returnmoney, s);
	}

	@Override
	public List<Returnmoney> getObject(Returnmoney returnmoney, String s) {
		// TODO Auto-generated method stub
		return super.getObject(returnmoney, s);
	}
}
