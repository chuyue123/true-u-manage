package com.cy.dao;

import java.util.List;

import com.cy.entity.Schedule;
import com.cy.util.GeneratorDaoImpl;

/**
 * 技术日程dao实现
 * 
 * @author fzp
 * 
 */
public class ScheduleDaoImpl extends GeneratorDaoImpl<Schedule> {

	@Override
	public boolean insert(Schedule t, String s) {
		// TODO Auto-generated method stub
		return super.insert(t, s);
	}

	@Override
	public boolean delete(Schedule t, String s) {
		// TODO Auto-generated method stub
		return super.delete(t, s);
	}

	@Override
	public boolean update(Schedule t, String s) {
		// TODO Auto-generated method stub
		return super.update(t, s);
	}

	@Override
	public List<Schedule> getObject(Schedule t, String s) {
		// TODO Auto-generated method stub
		return super.getObject(t, s);
	}
}
