package com.cy.dao;

import java.util.List;

import com.cy.entity.Contract;

import com.cy.util.GeneratorDaoImpl;

/**
 * 合同dao实现
 * 
 * @author fzp
 * 
 */
public class ContractDaoImpl extends GeneratorDaoImpl<Contract> {

	@Override
	public boolean insert(Contract contract, String s) {
		// TODO Auto-generated method stub
		return super.insert(contract, s);
	}

	@Override
	public boolean delete(Contract contract, String s) {
		// TODO Auto-generated method stub
		return super.delete(contract, s);
	}

	@Override
	public boolean update(Contract contract, String s) {
		// TODO Auto-generated method stub
		return super.update(contract, s);
	}

	@Override
	public List<Contract> getObject(Contract contract, String s) {
		// TODO Auto-generated method stub
		return super.getObject(contract, s);
	}
}
