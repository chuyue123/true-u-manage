package com.cy.dao;

import java.util.List;

import com.cy.entity.WeekPlan;
import com.cy.util.GeneratorDaoImpl;

/**
 * 周报dao实现
 * 
 * @author fzp
 * 
 */
public class WeekPlanDaoImpl extends GeneratorDaoImpl<WeekPlan> {

	@Override
	public boolean insert(WeekPlan weekPlan, String s) {
		// TODO Auto-generated method stub
		return super.insert(weekPlan, s);
	}

	@Override
	public boolean delete(WeekPlan weekPlan, String s) {
		// TODO Auto-generated method stub
		return super.delete(weekPlan, s);
	}

	@Override
	public boolean update(WeekPlan weekPlan, String s) {
		// TODO Auto-generated method stub
		return super.update(weekPlan, s);
	}

	@Override
	public List<WeekPlan> getObject(WeekPlan weekPlan, String s) {
		// TODO Auto-generated method stub
		return super.getObject(weekPlan, s);
	}
}
