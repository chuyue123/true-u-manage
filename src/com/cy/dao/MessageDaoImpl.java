package com.cy.dao;

import java.util.List;

import com.cy.entity.Message;
import com.cy.util.GeneratorDaoImpl;

/**
 * 信息dao实现
 * 
 * @author fzp
 * 
 */
public class MessageDaoImpl extends GeneratorDaoImpl<Message> {

	@Override
	public boolean insert(Message message, String s) {
		// TODO Auto-generated method stub
		return super.insert(message, s);
	}

	@Override
	public boolean delete(Message message, String s) {
		// TODO Auto-generated method stub
		return super.delete(message, s);
	}

	@Override
	public boolean update(Message message, String s) {
		// TODO Auto-generated method stub
		return super.update(message, s);
	}

	@Override
	public List<Message> getObject(Message message, String s) {
		// TODO Auto-generated method stub
		return super.getObject(message, s);
	}
}
