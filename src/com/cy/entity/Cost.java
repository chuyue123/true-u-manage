package com.cy.entity;

import java.io.Serializable;

/**
 * 花费（需要报销的费用）实体类
 * 
 * @author lin
 * 
 */
public class Cost implements Serializable {

	private Integer id;// 编号
	private Integer worktype; // 类型(技术或销售)
	private String receipts; // /小票
	private String invoice; // 发票
	private String type; // 类型（油费,饭费等）
	private Integer sum; // 金额
	private Integer workerid;// 申请者id
	private String trip;// 行程
	private String time;// 日期
	private Integer customerid;// 客户id
	private Integer projectnumber;// 项目编号
	private Integer customercontractid;// 客户联系方式id
	private String remark;// 备注
	private String destination;// 目的地
	private String strartplace;// 始发地

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getWorktype() {
		return worktype;
	}

	public void setWorktype(Integer worktype) {
		this.worktype = worktype;
	}

	public String getReceipts() {
		return receipts;
	}

	public void setReceipts(String receipts) {
		this.receipts = receipts;
	}

	public String getInvoice() {
		return invoice;
	}

	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getSum() {
		return sum;
	}

	public void setSum(Integer sum) {
		this.sum = sum;
	}

	public Integer getWorkerid() {
		return workerid;
	}

	public void setWorkerid(Integer workerid) {
		this.workerid = workerid;
	}

	public String getTrip() {
		return trip;
	}

	public void setTrip(String trip) {
		this.trip = trip;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Integer getCustomerid() {
		return customerid;
	}

	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}

	public Integer getProjectnumber() {
		return projectnumber;
	}

	public void setProjectnumber(Integer projectnumber) {
		this.projectnumber = projectnumber;
	}

	public Integer getCustomercontractid() {
		return customercontractid;
	}

	public void setCustomercontractid(Integer customercontractid) {
		this.customercontractid = customercontractid;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getStrartplace() {
		return strartplace;
	}

	public void setStrartplace(String strartplace) {
		this.strartplace = strartplace;
	}

	public Cost() {
		super();
		// TODO Auto-generated constructor stub
	}

}
