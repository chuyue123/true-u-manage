package com.cy.entity;

import java.io.Serializable;

/**
 * 周计划实体类
 * 
 * @author lin
 * 
 */
public class WeekPlan implements Serializable {

	private Integer id;// 编号
	private String description;// 周计划描述
	private String attachment;// 附件
	private Integer workerid;// 申请者id
	private Integer status;// 审核状态
	private Integer managerid;// 受审人id
	private String type;// 类型

	public WeekPlan(Integer id, String description, String attachment,
			Integer workerid, Integer status, Integer managerid, String type) {
		super();
		this.id = id;
		this.description = description;
		this.attachment = attachment;
		this.workerid = workerid;
		this.status = status;
		this.managerid = managerid;
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public Integer getWorkerid() {
		return workerid;
	}

	public void setWorkerid(Integer workerid) {
		this.workerid = workerid;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getManagerid() {
		return managerid;
	}

	public void setManagerid(Integer managerid) {
		this.managerid = managerid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public WeekPlan() {
		super();
		// TODO Auto-generated constructor stub
	}
}
