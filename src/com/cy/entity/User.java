package com.cy.entity;

import java.io.Serializable;

/**
 * 用户实体类
 * 
 * @author lin
 * 
 */
public class User implements Serializable {

	private Integer id;// 数据库自动增长
	private String name;// 姓名注册
	private String password;// 密码
	private String sex;// 性别
	private String email;// 企业邮箱
	private String tel;// 手机或者电话号码
	private String role;// 角色
	private String status;// 状态
	private String photo;// 相片路径

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(Integer id, String name, String password, String sex, String email,
			String tel, String role, String status, String photo) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.sex = sex;
		this.email = email;
		this.tel = tel;
		this.role = role;
		this.status = status;
		this.photo = photo;
	}
}
