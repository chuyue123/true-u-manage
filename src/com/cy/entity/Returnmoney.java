package com.cy.entity;

import java.io.Serializable;

/**
 * 回款实体类
 * 
 * @author lin
 * 
 */
public class Returnmoney implements Serializable {

	private Integer id;// 编号
	private String money;// 金额
	private String invoice;// 发票
	private Integer invoicenumber;// 发票编号
	private Integer contractid;// 合同id

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getInvoice() {
		return invoice;
	}

	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}

	public Integer getInvoicenumber() {
		return invoicenumber;
	}

	public void setInvoicenumber(Integer invoicenumber) {
		this.invoicenumber = invoicenumber;
	}

	public Integer getContractid() {
		return contractid;
	}

	public void setContractid(Integer contractid) {
		this.contractid = contractid;
	}

	public Returnmoney(Integer id, String money, String invoice,
			Integer invoicenumber, Integer contractid) {
		super();
		this.id = id;
		this.money = money;
		this.invoice = invoice;
		this.invoicenumber = invoicenumber;
		this.contractid = contractid;
	}

	public Returnmoney() {
		super();
		// TODO Auto-generated constructor stub
	}
}
