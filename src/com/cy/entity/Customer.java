package com.cy.entity;

import java.io.Serializable;

/**
 * 客户实体类
 * 
 * @author lin
 * 
 */
public class Customer implements Serializable {

	private Integer id;// 编号
	private String name;// 客户名称
	private String industry;// 行业
	private String product;// 产品
	private Integer saler;// 销售代表
	private String sproduct;// 报备产品
	private Integer sstatus;// 报备状态
	private String designernumber;// 设计人员人数
	private Integer visitnumber;// 拜访次数
	private String erp;// 所使用ERP
	private String pdm;// 所使用PDM
	private String createtime;// 创建日期
	private Integer number;// CRM系统中对应编号
	private Integer visited;// 是否拜访
	private String address;// 地址
	private String description;// 客户说明
	private Integer listed;// 是否上市公司
	private Integer level;// 级别
	private String maindrawsoftware;// 主要三维软件
	private Integer creatorid;// 创建者id

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public Integer getSaler() {
		return saler;
	}

	public void setSaler(Integer saler) {
		this.saler = saler;
	}

	public String getSproduct() {
		return sproduct;
	}

	public void setSproduct(String sproduct) {
		this.sproduct = sproduct;
	}

	public Integer getSstatus() {
		return sstatus;
	}

	public void setSstatus(Integer sstatus) {
		this.sstatus = sstatus;
	}

	public String getDesignernumber() {
		return designernumber;
	}

	public void setDesignernumber(String designernumber) {
		this.designernumber = designernumber;
	}

	public Integer getVisitnumber() {
		return visitnumber;
	}

	public void setVisitnumber(Integer visitnumber) {
		this.visitnumber = visitnumber;
	}

	public String getErp() {
		return erp;
	}

	public void setErp(String erp) {
		this.erp = erp;
	}

	public String getPdm() {
		return pdm;
	}

	public void setPdm(String pdm) {
		this.pdm = pdm;
	}

	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getVisited() {
		return visited;
	}

	public void setVisited(Integer visited) {
		this.visited = visited;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getListed() {
		return listed;
	}

	public void setListed(Integer listed) {
		this.listed = listed;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getMaindrawsoftware() {
		return maindrawsoftware;
	}

	public void setMaindrawsoftware(String maindrawsoftware) {
		this.maindrawsoftware = maindrawsoftware;
	}

	public Integer getCreatorid() {
		return creatorid;
	}

	public void setCreatorid(Integer creatorid) {
		this.creatorid = creatorid;
	}

	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Customer(Integer id, String name, String industry, String product,
			Integer saler, String sproduct, Integer sstatus,
			String designernumber, Integer visitnumber, String erp, String pdm,
			String createtime, Integer number, Integer visited, String address,
			String description, Integer listed, Integer level,
			String maindrawsoftware, Integer creatorid) {
		super();
		this.id = id;
		this.name = name;
		this.industry = industry;
		this.product = product;
		this.saler = saler;
		this.sproduct = sproduct;
		this.sstatus = sstatus;
		this.designernumber = designernumber;
		this.visitnumber = visitnumber;
		this.erp = erp;
		this.pdm = pdm;
		this.createtime = createtime;
		this.number = number;
		this.visited = visited;
		this.address = address;
		this.description = description;
		this.listed = listed;
		this.level = level;
		this.maindrawsoftware = maindrawsoftware;
		this.creatorid = creatorid;
	}
}
