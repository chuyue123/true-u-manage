package com.cy.entity;


import java.io.Serializable;

/**
 * 合同实体类
 * 
 * @author fzp
 * 
 */
public class Contract implements Serializable {

	private Integer id;// 编号
	private Integer number; // 合同编号
	private Integer points;// /点数
	private String monney;// 金额
	private Integer Contractlinkprodcutid;// 产品与合同链接id
	private String service;// 服务
	private Integer manday;// 人天
	private Integer projectnumber;// 项目编号
	private Integer customerid;// 客户id

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public String getMonney() {
		return monney;
	}

	public void setMonney(String monney) {
		this.monney = monney;
	}

	public Integer getContractlinkprodcutid() {
		return Contractlinkprodcutid;
	}

	public void setContractlinkprodcutid(Integer contractlinkprodcutid) {
		Contractlinkprodcutid = contractlinkprodcutid;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public Integer getManday() {
		return manday;
	}

	public void setManday(Integer manday) {
		this.manday = manday;
	}

	public Integer getProjectnumber() {
		return projectnumber;
	}

	public void setProjectnumber(Integer projectnumber) {
		this.projectnumber = projectnumber;
	}

	public Integer getCustomerid() {
		return customerid;
	}

	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}

	public Contract() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Contract(Integer id, Integer number, Integer points, String monney,
			Integer contractlinkprodcutid, String service, Integer manday,
			Integer projectnumber, Integer customerid) {
		super();
		this.id = id;
		this.number = number;
		this.points = points;
		this.monney = monney;
		Contractlinkprodcutid = contractlinkprodcutid;
		this.service = service;
		this.manday = manday;
		this.projectnumber = projectnumber;
		this.customerid = customerid;
	}
}
