package com.cy.entity;

import java.io.Serializable;

/**
 * 客户级别实体类
 * 
 * @author lin
 * 
 */
public class CustomerLevel implements Serializable {

	private Integer id;// 编号
	private Integer customerid;// 客户id
	private Integer newlevel;// 新级别
	private Integer oldlevel;// 原先级别
	private String reason;// 变更原因

	public CustomerLevel(Integer id, Integer customerid, Integer newlevel,
			Integer oldlevel, String reason) {
		super();
		this.id = id;
		this.customerid = customerid;
		this.newlevel = newlevel;
		this.oldlevel = oldlevel;
		this.reason = reason;
	}

	public CustomerLevel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomerid() {
		return customerid;
	}

	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}

	public Integer getNewlevel() {
		return newlevel;
	}

	public void setNewlevel(Integer newlevel) {
		this.newlevel = newlevel;
	}

	public Integer getOldlevel() {
		return oldlevel;
	}

	public void setOldlevel(Integer oldlevel) {
		this.oldlevel = oldlevel;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
