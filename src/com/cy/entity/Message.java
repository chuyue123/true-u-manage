package com.cy.entity;

import java.io.Serializable;

/**
 * 信息实体类
 * 
 * @author lin
 * 
 */
public class Message implements Serializable {

	private Integer id;// 编号
	private String description;// 信息描述
	private String type;// 信息类型
	private String createtime;// 信息时间
	private Integer status;// 信息状态
	private String title;// 标题
	private Integer recipientid;// 接受者id
	private Integer sender;// 发信人id

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getRecipientid() {
		return recipientid;
	}

	public void setRecipientid(Integer recipientid) {
		this.recipientid = recipientid;
	}

	public Integer getSender() {
		return sender;
	}

	public void setSender(Integer sender) {
		this.sender = sender;
	}

	public Message() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Message(Integer id, String description, String type,
			String createtime, Integer status, String title,
			Integer recipientid, Integer sender) {
		super();
		this.id = id;
		this.description = description;
		this.type = type;
		this.createtime = createtime;
		this.status = status;
		this.title = title;
		this.recipientid = recipientid;
		this.sender = sender;
	}
}
