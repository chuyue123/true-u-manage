package com.cy.entity;

import java.io.Serializable;

/**
 * 技术日程实体类
 * 
 * @author lin
 * 
 */
public class Schedule implements Serializable {

	private Integer id;// 编号
	private Integer projectnumber;// 项目编号
	private String workcontent;// 工作内容
	private String remark;// 备注
	private String time;// 日期
	private Integer worktime;// 工时
	private Integer workerid;// 申请者id
	private String worktype;// 工作类型

	public Schedule(Integer id, Integer projectnumber, String workcontent,
			String remark, String time, Integer worktime, Integer workerid,
			String worktype) {
		super();
		this.id = id;
		this.projectnumber = projectnumber;
		this.workcontent = workcontent;
		this.remark = remark;
		this.time = time;
		this.worktime = worktime;
		this.workerid = workerid;
		this.worktype = worktype;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProjectnumber() {
		return projectnumber;
	}

	public void setProjectnumber(Integer projectnumber) {
		this.projectnumber = projectnumber;
	}

	public String getWorkcontent() {
		return workcontent;
	}

	public void setWorkcontent(String workcontent) {
		this.workcontent = workcontent;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Integer getWorktime() {
		return worktime;
	}

	public void setWorktime(Integer worktime) {
		this.worktime = worktime;
	}

	public Integer getWorkerid() {
		return workerid;
	}

	public void setWorkerid(Integer workerid) {
		this.workerid = workerid;
	}

	public String getWorktype() {
		return worktype;
	}

	public void setWorktype(String worktype) {
		this.worktype = worktype;
	}

	public Schedule() {
		super();
		// TODO Auto-generated constructor stub
	}

}
