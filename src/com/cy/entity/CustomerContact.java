package com.cy.entity;

import java.io.Serializable;

/**
 * 客户联系方式
 * @author lin
 *
 */
public class CustomerContact implements Serializable {

	private Integer id;//编号
	private String name;//客户姓名
	private String department;//部门
	private String position;//职务
	private String QQ;//QQ号码
	private String photo;//相片
	private String tel;//电话号码
	private String email;//邮箱
	private String status;//状态
	private Integer createorid;//创建者id
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getQQ() {
		return QQ;
	}

	public void setQQ(String qQ) {
		QQ = qQ;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getCreateorid() {
		return createorid;
	}

	public void setCreateorid(Integer createorid) {
		this.createorid = createorid;
	}

	public CustomerContact() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CustomerContact(Integer id, String name, String department,
			String position, String qQ, String photo, String tel, String email,
			String status, Integer createorid) {
		super();
		this.id = id;
		this.name = name;
		this.department = department;
		this.position = position;
		QQ = qQ;
		this.photo = photo;
		this.tel = tel;
		this.email = email;
		this.status = status;
		this.createorid = createorid;
	}
}
