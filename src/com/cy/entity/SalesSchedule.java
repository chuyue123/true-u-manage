package com.cy.entity;

import java.io.Serializable;

/**
 * 销售日程
 * @author lin
 *
 */
public class SalesSchedule implements Serializable {
	
	private Integer id;//编号
	private String workcontent;///工作内容
	private String remark;//备注
	private String time;//日期
	private Integer worktime;//工时
	private Integer workerid;//申请者id
	private String worktype;//工作类型
	private String  signin;//签到
	private Integer customerid;//客户id
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getWorkcontent() {
		return workcontent;
	}
	public void setWorkcontent(String workcontent) {
		this.workcontent = workcontent;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public Integer getWorktime() {
		return worktime;
	}
	public void setWorktime(Integer worktime) {
		this.worktime = worktime;
	}
	public Integer getWorkerid() {
		return workerid;
	}
	public void setWorkerid(Integer workerid) {
		this.workerid = workerid;
	}
	public String getWorktype() {
		return worktype;
	}
	public void setWorktype(String worktype) {
		this.worktype = worktype;
	}
	public String getSignin() {
		return signin;
	}
	public void setSignin(String signin) {
		this.signin = signin;
	}
	public Integer getCustomerid() {
		return customerid;
	}
	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}
	public SalesSchedule(Integer id, String workcontent, String remark,
			String time, Integer worktime, Integer workerid, String worktype,
			String signin, Integer customerid) {
		super();
		this.id = id;
		this.workcontent = workcontent;
		this.remark = remark;
		this.time = time;
		this.worktime = worktime;
		this.workerid = workerid;
		this.worktype = worktype;
		this.signin = signin;
		this.customerid = customerid;
	}
	public SalesSchedule() {
		super();
		// TODO Auto-generated constructor stub
	}	
}
