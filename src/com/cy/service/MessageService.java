package com.cy.service;

import java.util.List;
import com.cy.entity.Message;
import com.cy.util.GeneratorDaoImpl;
import com.cy.util.IGenerator;

/**
 * 信息service实现
 * 
 * @author fzp
 * @param <T>
 * 
 */
public class MessageService implements IGenerator<Message> {

	private GeneratorDaoImpl<Message> messageDaoImpl;

	public GeneratorDaoImpl<Message> getMessageDaoImpl() {
		return messageDaoImpl;
	}

	public void setMessageDaoImpl(GeneratorDaoImpl<Message> messageDaoImpl) {
		this.messageDaoImpl = messageDaoImpl;
	}

	@Override
	public boolean insert(Message message, String s) {
		// TODO Auto-generated method stub
		return this.messageDaoImpl.insert(message, s);
	}

	@Override
	public boolean delete(Message message, String s) {
		// TODO Auto-generated method stub
		return this.messageDaoImpl.delete(message, s);
	}

	@Override
	public boolean update(Message message, String s) {
		// TODO Auto-generated method stub
		return this.messageDaoImpl.update(message, s);
	}

	@Override
	public List<Message> getObject(Message message, String s) {
		// TODO Auto-generated method stub
		return this.messageDaoImpl.getObject(message, s);
	}

}
