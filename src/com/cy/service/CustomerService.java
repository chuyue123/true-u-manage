
package com.cy.service;

import java.util.List;


import com.cy.entity.Customer;
import com.cy.util.IGenerator;

public class CustomerService implements IGenerator<Customer> {

	private IGenerator<Customer> CustomerDaoImpl;
	
	public IGenerator<Customer> getCustomerDaoImpl() {
		return CustomerDaoImpl;
	}

	public void setCustomerDaoImpl(IGenerator<Customer> customerDaoImpl) {
		CustomerDaoImpl = customerDaoImpl;
	}

	@Override
	public boolean insert(Customer customer, String s) {
		 
		return this.CustomerDaoImpl.insert(customer, s);
	}

	@Override
	public boolean delete(Customer customer, String s) {
		 
		return this.CustomerDaoImpl.delete(customer, s);
	}

	@Override
	public boolean update(Customer customer, String s) {
		 
		return this.CustomerDaoImpl.update(customer, s);
	}

	@Override
	public List<Customer> getObject(Customer customer, String s) {
		 
		return this.CustomerDaoImpl.getObject(customer, s);
	}

}

