package com.cy.service;

import java.util.List;

import com.cy.entity.User;
import com.cy.util.IGenerator;

public class UserService implements IGenerator<User> {

	private IGenerator<User> userDaoImpl;

	public IGenerator<User> getUserDaoImpl() {
		return userDaoImpl;
	}

	public void setUserDaoImpl(IGenerator<User> userDaoImpl) {
		this.userDaoImpl = userDaoImpl;
	}

	@Override
	public boolean insert(User user, String s) {
		// TODO Auto-generated method stub
		return this.userDaoImpl.insert(user, s);
	}

	@Override
	public boolean delete(User user, String s) {
		// TODO Auto-generated method stub
		return this.userDaoImpl.delete(user, s);
	}

	@Override
	public boolean update(User user, String s) {
		// TODO Auto-generated method stub
		return this.userDaoImpl.update(user, s);
	}

	@Override
	public List<User> getObject(User user, String s) {
		// TODO Auto-generated method stub
		return this.userDaoImpl.getObject(user, s);
	}
}
