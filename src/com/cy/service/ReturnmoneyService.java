package com.cy.service;

import java.util.List;

import com.cy.entity.Returnmoney;
import com.cy.util.IGenerator;

/**
 * 回款service实现
 * 
 * @author fzp
 * 
 */
public class ReturnmoneyService implements IGenerator<Returnmoney> {

	private IGenerator<Returnmoney> returnmoneyDaoImpl;

	public IGenerator<Returnmoney> getReturnmoneyDaoImpl() {
		return returnmoneyDaoImpl;
	}

	public void setReturnmoneyDaoImpl(IGenerator<Returnmoney> returnmoneyDaoImpl) {
		this.returnmoneyDaoImpl = returnmoneyDaoImpl;
	}

	@Override
	public boolean insert(Returnmoney returnmoney, String s) {
		// TODO Auto-generated method stub
		return this.returnmoneyDaoImpl.insert(returnmoney, s);
	}

	@Override
	public boolean delete(Returnmoney returnmoney, String s) {
		// TODO Auto-generated method stub
		return this.returnmoneyDaoImpl.delete(returnmoney, s);
	}

	@Override
	public boolean update(Returnmoney returnmoney, String s) {
		// TODO Auto-generated method stub
		return this.returnmoneyDaoImpl.update(returnmoney, s);
	}

	@Override
	public List<Returnmoney> getObject(Returnmoney returnmoney, String s) {
		// TODO Auto-generated method stub
		return this.returnmoneyDaoImpl.getObject(returnmoney, s);
	}

}
