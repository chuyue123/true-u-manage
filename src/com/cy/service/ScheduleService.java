package com.cy.service;

import java.util.List;
import com.cy.entity.Schedule;
import com.cy.util.IGenerator;

/**
 * 技术日程service实现
 * 
 * @author fzp
 * 
 */
public class ScheduleService implements IGenerator<Schedule> {

	private IGenerator<Schedule> scheduleDaoImpl;

	public IGenerator<Schedule> getScheduleDaoImpl() {
		return scheduleDaoImpl;
	}

	public void setScheduleDaoImpl(IGenerator<Schedule> scheduleDaoImpl) {
		this.scheduleDaoImpl = scheduleDaoImpl;
	}

	@Override
	public boolean insert(Schedule schedule, String s) {
		// TODO Auto-generated method stub
		return this.scheduleDaoImpl.insert(schedule, s);
	}

	@Override
	public boolean delete(Schedule schedule, String s) {
		// TODO Auto-generated method stub
		return this.scheduleDaoImpl.delete(schedule, s);
	}

	@Override
	public boolean update(Schedule schedule, String s) {
		// TODO Auto-generated method stub
		return this.scheduleDaoImpl.update(schedule, s);
	}

	@Override
	public List<Schedule> getObject(Schedule schedule, String s) {
		// TODO Auto-generated method stub
		return this.scheduleDaoImpl.getObject(schedule, s);
	}

}
