package com.cy.service;

import java.util.List;

import com.cy.entity.WeekPlan;
import com.cy.util.IGenerator;

/**
 * 周报service实现
 * 
 * @author fzp
 * 
 */
public class WeekPlanService implements IGenerator<WeekPlan> {

	private IGenerator<WeekPlan> weekPlanDaoImpl;

	public IGenerator<WeekPlan> getWeekPlanDaoImpl() {
		return weekPlanDaoImpl;
	}

	public void setWeekPlanDaoImpl(IGenerator<WeekPlan> weekPlanDaoImpl) {
		this.weekPlanDaoImpl = weekPlanDaoImpl;
	}

	@Override
	public boolean insert(WeekPlan weekPlan, String s) {
		// TODO Auto-generated method stub
		return this.weekPlanDaoImpl.insert(weekPlan, s);
	}

	@Override
	public boolean delete(WeekPlan weekPlan, String s) {
		// TODO Auto-generated method stub
		return this.weekPlanDaoImpl.delete(weekPlan, s);
	}

	@Override
	public boolean update(WeekPlan weekPlan, String s) {
		// TODO Auto-generated method stub
		return this.weekPlanDaoImpl.update(weekPlan, s);
	}

	@Override
	public List<WeekPlan> getObject(WeekPlan weekPlan, String s) {
		// TODO Auto-generated method stub
		return this.weekPlanDaoImpl.getObject(weekPlan, s);
	}


}
