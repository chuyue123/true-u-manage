package com.cy.service;

import java.util.List;

import com.cy.entity.Cost;
import com.cy.util.IGenerator;

/**
 * 花费（需要报销的费用）service 实现
 * 
 * @author fzp
 * 
 */
public class CostService implements IGenerator<Cost> {

	private IGenerator<Cost> costDaoImpl;

	public IGenerator<Cost> getCostDaoImpl() {
		return costDaoImpl;
	}

	public void setCostDaoImpl(IGenerator<Cost> costDaoImpl) {
		this.costDaoImpl = costDaoImpl;
	}

	@Override
	public boolean insert(Cost cost, String s) {
		// TODO Auto-generated method stub
		return this.costDaoImpl.insert(cost, s);
	}

	@Override
	public boolean delete(Cost cost, String s) {
		// TODO Auto-generated method stub
		return this.costDaoImpl.delete(cost, s);
	}

	@Override
	public boolean update(Cost cost, String s) {
		// TODO Auto-generated method stub
		return this.costDaoImpl.update(cost, s);
	}

	@Override
	public List<Cost> getObject(Cost cost, String s) {
		// TODO Auto-generated method stub
		return this.costDaoImpl.getObject(cost, s);
	}

}
