package com.cy.service;

import java.util.List;

import com.cy.entity.SalesSchedule;
import com.cy.util.IGenerator;

public class SalesScheduleService implements IGenerator<SalesSchedule> {
	
	private IGenerator<SalesSchedule> SalesScheduleDaoImpl;
	
	public IGenerator<SalesSchedule> getSalesScheduleDaoImpl() {
		return SalesScheduleDaoImpl;
	}

	public void setSalesScheduleDaoImpl(
			IGenerator<SalesSchedule> salesScheduleDaoImpl) {
		SalesScheduleDaoImpl = salesScheduleDaoImpl;
	}

	@Override
	public boolean insert(SalesSchedule t, String s) {
		// TODO Auto-generated method stub
		return  this.SalesScheduleDaoImpl.insert(t, s);
	}

	@Override
	public boolean delete(SalesSchedule t, String s) {
		// TODO Auto-generated method stub
		return this.SalesScheduleDaoImpl.delete(t, s);
	}

	@Override
	public boolean update(SalesSchedule t, String s) {
		// TODO Auto-generated method stub
		return this.SalesScheduleDaoImpl.update(t, s);
	}

	@Override
	public List<SalesSchedule> getObject(SalesSchedule t, String s) {
		// TODO Auto-generated method stub
		return this.SalesScheduleDaoImpl.getObject(t, s);
	}

}
