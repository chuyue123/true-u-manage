package com.cy.service;

import java.util.List;

import com.cy.entity.Contract;

import com.cy.util.IGenerator;

/**
 * 合同service实现
 * 
 * @author fzp
 * 
 */
public class ContractService implements IGenerator<Contract> {

	private IGenerator<Contract> contractDaoImpl;

	public IGenerator<Contract> getContractDaoImpl() {
		return contractDaoImpl;
	}

	public void setContractDaoImpl(IGenerator<Contract> contractDaoImpl) {
		this.contractDaoImpl = contractDaoImpl;
	}

	@Override
	public boolean insert(Contract contract, String s) {
		// TODO Auto-generated method stub
		return this.contractDaoImpl.insert(contract, s);
	}

	@Override
	public boolean delete(Contract contract, String s) {
		// TODO Auto-generated method stub
		return this.contractDaoImpl.delete(contract, s);
	}

	@Override
	public boolean update(Contract contract, String s) {
		// TODO Auto-generated method stub
		return this.contractDaoImpl.update(contract, s);
	}

	@Override
	public List<Contract> getObject(Contract contract, String s) {
		// TODO Auto-generated method stub
		return this.contractDaoImpl.getObject(contract, s);
	}

}
