package com.cy.service;

import java.util.List;

import com.cy.entity.CustomerContact;
import com.cy.util.IGenerator;

public class CustomerContactService implements IGenerator<CustomerContact> {

	private IGenerator<CustomerContact> contactDaoImpl;

	public IGenerator<CustomerContact> getContactDaoImpl() {
		return contactDaoImpl;
	}

	public void setContactDaoImpl(IGenerator<CustomerContact> contactDaoImpl) {
		this.contactDaoImpl = contactDaoImpl;
	}

	@Override
	public boolean insert(CustomerContact contact, String s) {
		// TODO Auto-generated method stub
		return this.contactDaoImpl.insert(contact, s);
	}

	@Override
	public boolean delete(CustomerContact contact, String s) {
		// TODO Auto-generated method stub
		return this.contactDaoImpl.delete(contact, s);
	}

	@Override
	public boolean update(CustomerContact contact, String s) {
		// TODO Auto-generated method stub
		return this.contactDaoImpl.update(contact, s);
	}

	@Override
	public List<CustomerContact> getObject(CustomerContact contact, String s) {
		// TODO Auto-generated method stub
		return this.contactDaoImpl.getObject(contact, s);
	}

}
