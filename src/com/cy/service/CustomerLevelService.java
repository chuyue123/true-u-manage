package com.cy.service;

import java.util.List;

import com.cy.entity.CustomerLevel;
import com.cy.util.GeneratorDaoImpl;
import com.cy.util.IGenerator;

/**
 * 客户级别service实现
 * 
 * @author fzp
 * 
 */
public class CustomerLevelService implements IGenerator<CustomerLevel> {

	private GeneratorDaoImpl<CustomerLevel> levelDaoImpl;

	public GeneratorDaoImpl<CustomerLevel> getLevelDaoImpl() {
		return levelDaoImpl;
	}

	public void setLevelDaoImpl(GeneratorDaoImpl<CustomerLevel> levelDaoImpl) {
		this.levelDaoImpl = levelDaoImpl;
	}

	@Override
	public boolean insert(CustomerLevel customerLevel, String s) {
		// TODO Auto-generated method stub
		return this.levelDaoImpl.insert(customerLevel, s);
	}

	@Override
	public boolean delete(CustomerLevel customerLevel, String s) {
		// TODO Auto-generated method stub
		return this.levelDaoImpl.delete(customerLevel, s);
	}

	@Override
	public boolean update(CustomerLevel customerLevel, String s) {
		// TODO Auto-generated method stub
		return this.levelDaoImpl.update(customerLevel, s);
	}

	@Override
	public List<CustomerLevel> getObject(CustomerLevel customerLevel, String s) {
		// TODO Auto-generated method stub
		return this.levelDaoImpl.getObject(customerLevel, s);
	}

}
