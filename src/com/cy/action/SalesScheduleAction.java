package com.cy.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import net.sf.json.JSONObject;

import com.cy.entity.Customer;
import com.cy.entity.SalesSchedule;

import com.cy.util.IGenerator;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class SalesScheduleAction extends ActionSupport implements
		ModelDriven<SalesSchedule>, Action {

	private SalesSchedule salesSchedule;
	private Customer customer;
	private IGenerator<SalesSchedule> SalesScheduleService;
	private IGenerator<Customer> customerService;
	private List<SalesSchedule> listSalesSchedules;
	private List<Customer> listCustomers;
	private JSONObject jo = new JSONObject();

	public List<Customer> getListCustomers() {
		return listCustomers;
	}

	public void setListCustomers(List<Customer> listCustomers) {
		this.listCustomers = listCustomers;
	}

	public IGenerator<Customer> getCustomerService() {
		return customerService;
	}

	public void setCustomerService(IGenerator<Customer> customerService) {
		this.customerService = customerService;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public JSONObject getJo() {
		return jo;
	}

	public void setJo(JSONObject jo) {
		this.jo = jo;
	}

	public SalesSchedule getSalesSchedule() {
		return salesSchedule;
	}

	public void setSalesSchedule(SalesSchedule salesSchedule) {
		this.salesSchedule = salesSchedule;
	}

	public IGenerator<SalesSchedule> getSalesScheduleService() {
		return SalesScheduleService;
	}

	public void setSalesScheduleService(
			IGenerator<SalesSchedule> salesScheduleService) {
		SalesScheduleService = salesScheduleService;
	}

	public List<SalesSchedule> getListSalesSchedules() {
		return listSalesSchedules;
	}

	public void setListSalesSchedules(List<SalesSchedule> listSalesSchedules) {
		this.listSalesSchedules = listSalesSchedules;
	}

	@Override
	public SalesSchedule getModel() {
		return salesSchedule;
	}

	/**
	 * app输入销售日程信息
	 * 
	 * @return jo
	 */
	public String insertSale() {
		if (salesSchedule.getWorkcontent() == null
				|| salesSchedule.getCustomerid() == null
				|| salesSchedule.getSignin() == null
				|| salesSchedule.getTime() == null
				|| salesSchedule.getWorkerid() == null
				|| salesSchedule.getWorktime() == null
				|| salesSchedule.getWorktype() == null) {
			jo.put("success", true);
			jo.put("msg", "提交不能为空！！！");
			return "jo";
		}
		customer.setId(salesSchedule.getCustomerid());
		listCustomers=customerService.getObject(customer, "Customer.select");
		if(listCustomers!=null){
			jo.put("success", true);
			jo.put("msg", "客户不存在，请先录入客户信息！！！");
			return "jo";
		}
		Date date=new Date(salesSchedule.getTime().toString());
		SimpleDateFormat dateFormat=new SimpleDateFormat("z yyyy-MM-dd E  HH:mm:ss");
		String time = dateFormat.format(date);
		salesSchedule.setTime(time);
		boolean flag = SalesScheduleService
				.insert(salesSchedule, "Sale.insert");
		if (!flag) {
			jo.put("success", true);
			jo.put("msg", "数据提交失败！！！");
			return "jo";
		}
		jo.put("success", true);
		jo.put("msg", "");
		return "jo";
	}

	public String insertSalesSchedule() {
		String rse = "err";
		ServletActionContext.getResponse().setHeader(
				"Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept");
		ServletActionContext.getResponse().setHeader(
				"Access-Control-Allow-Origin", "*");
		this.jo = new JSONObject();
		if (salesSchedule != null) {
			boolean flag = SalesScheduleService.delete(salesSchedule,
					"Sale.insert");
			if (flag) {
				// 加载显示功能

				// this.jo.put("lng", lng);
				return SUCCESS;
			} else {
				return rse;
			}
		}
		return rse;
	}

	public String deleteSalesSchedule() {
		String rse = "err";
		if (salesSchedule != null) {
			boolean flag = SalesScheduleService.delete(salesSchedule,
					"Sale.delete");
			if (flag) {
				// 加载显示功能

				return SUCCESS;
			} else {
				return rse;
			}
		}
		return rse;
	}

	public String updateSalesSchedule() {
		String rse = "err";
		if (salesSchedule != null) {
			boolean flag = SalesScheduleService.update(salesSchedule,
					"Sale.update");
			if (flag) {
				// 加载显示功能

				return SUCCESS;
			} else {
				return rse;
			}
		}
		return rse;
	}

	// 条件获取所有信息
	public String selectSalesSchedule() {
		String rse = "err";
		if (salesSchedule != null) {
			listSalesSchedules = SalesScheduleService.getObject(salesSchedule,
					"Sale.select");
			if (listSalesSchedules != null & listSalesSchedules.size() != 0) {
				// 加载显示功能
				// 封装jonson的对象 app传值

				return SUCCESS;
			} else {
				return rse;
			}
		}
		return rse;
	}

	@Override
	public String getSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDelete() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSelectApp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelectpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDeletepp() {
		// TODO Auto-generated method stub
		return null;
	}
}
