package com.cy.action;

import java.util.List;

import com.cy.entity.CustomerLevel;
import com.cy.util.IGenerator;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

/**
 * 客户级别action
 * 
 * @author fzp
 * 
 */
public class CustomerLevelAction extends ActionSupport implements
		ModelDriven<CustomerLevel>, Action {

	private CustomerLevel level;
	private IGenerator<CustomerLevel> levelService;
	private List<CustomerLevel> listLevels;

	@Override
	public CustomerLevel getModel() {
		// TODO Auto-generated method stub
		return level;
	}

	public CustomerLevel getLevel() {
		return level;
	}

	public void setLevel(CustomerLevel level) {
		this.level = level;
	}

	public IGenerator<CustomerLevel> getLevelService() {
		return levelService;
	}

	public void setLevelService(IGenerator<CustomerLevel> levelService) {
		this.levelService = levelService;
	}

	public List<CustomerLevel> getListLevels() {
		return listLevels;
	}

	public void setListLevels(List<CustomerLevel> listLevels) {
		this.listLevels = listLevels;
	}

	/**
	 * 添加级别
	 * 
	 * @return 成功页面
	 */
	public String insertCuslevel() {
		boolean flag = levelService.insert(level, "insert");
		if (flag) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 删除级别
	 * 
	 * @return 成功页面
	 */
	public String deleteCuslevel() {
		boolean flag = levelService.delete(level, "delete");
		if (flag) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 修改合同
	 * 
	 * @return 修改页面
	 */
	public String updateCuslevel() {
		listLevels = levelService.getObject(level, "select");
		if (listLevels != null && listLevels.size() == 1) {
			level = listLevels.get(0);
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 保存修改
	 * 
	 * @return 成功页面
	 */
	public String saveUpdate() {
		boolean flag = levelService.update(level, "update");
		if (flag) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 查询级别
	 * 
	 * @return listLevels，页面
	 */
	public String selectCuslevel() {
		listLevels = levelService.getObject(level, "select");
		if (listLevels != null) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	@Override
	public String getSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDelete() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSelectApp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelectpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDeletepp() {
		// TODO Auto-generated method stub
		return null;
	}
}
