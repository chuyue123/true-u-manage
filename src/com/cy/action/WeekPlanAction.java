package com.cy.action;

import java.util.List;

import com.cy.entity.WeekPlan;
import com.cy.util.IGenerator;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class WeekPlanAction extends ActionSupport implements
		ModelDriven<WeekPlan>, Action {

	private WeekPlan weekPlan;
	private IGenerator<WeekPlan> weekPlanService;
	private List<WeekPlan> listWeekPlans;

	public IGenerator<WeekPlan> getWeekPlanService() {
		return weekPlanService;
	}

	public void setWeekPlanService(IGenerator<WeekPlan> weekPlanService) {
		this.weekPlanService = weekPlanService;
	}

	public WeekPlan getWeekPlan() {
		return weekPlan;
	}

	public void setWeekPlan(WeekPlan weekPlan) {
		this.weekPlan = weekPlan;
	}

	public List<WeekPlan> getListWeekPlans() {
		return listWeekPlans;
	}

	public void setListWeekPlans(List<WeekPlan> listWeekPlans) {
		this.listWeekPlans = listWeekPlans;
	}

	@Override
	public WeekPlan getModel() {
		// TODO Auto-generated method stub
		return weekPlan;
	}


	@Override
	public String getSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDelete() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSelectApp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelectpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDeletepp() {
		// TODO Auto-generated method stub
		return null;
	}
}
