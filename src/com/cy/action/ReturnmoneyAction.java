package com.cy.action;

import java.util.List;

import com.cy.entity.Returnmoney;
import com.cy.util.IGenerator;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

/**
 * 回款action
 * 
 * @author fzp
 * 
 */
public class ReturnmoneyAction extends ActionSupport implements
		ModelDriven<Returnmoney>, Action {

	private Returnmoney returnmoney;
	private IGenerator<Returnmoney> returnmoneyService;
	private List<Returnmoney> listReturnmoneys;

	@Override
	public Returnmoney getModel() {
		// TODO Auto-generated method stub
		return returnmoney;
	}

	public Returnmoney getReturnmoney() {
		return returnmoney;
	}

	public void setReturnmoney(Returnmoney returnmoney) {
		this.returnmoney = returnmoney;
	}

	public IGenerator<Returnmoney> getReturnmoneyService() {
		return returnmoneyService;
	}

	public void setReturnmoneyService(IGenerator<Returnmoney> returnmoneyService) {
		this.returnmoneyService = returnmoneyService;
	}

	public List<Returnmoney> getListReturnmoneys() {
		return listReturnmoneys;
	}

	public void setListReturnmoneys(List<Returnmoney> listReturnmoneys) {
		this.listReturnmoneys = listReturnmoneys;
	}

	/**
	 * 添加回款
	 * 
	 * @return 成功页面
	 */
	public String insertReturnmoney() {
		boolean flag = returnmoneyService.insert(returnmoney, "insert");
		if (flag) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 删除回款
	 * 
	 * @return 成功页面
	 */
	public String deleteReturnmoney() {
		boolean flag = returnmoneyService.delete(returnmoney, "delete");
		if (flag) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 修改回款
	 * 
	 * @return 修改页面
	 */
	public String updateReturnmoney() {
		listReturnmoneys = returnmoneyService.getObject(returnmoney, "select");
		if (listReturnmoneys != null && listReturnmoneys.size() == 1) {
			returnmoney = listReturnmoneys.get(0);
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 保存修改
	 * 
	 * @return 成功页面
	 */
	public String saveUpdate() {
		boolean flag = returnmoneyService.update(returnmoney, "update");
		if (flag) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 查询合同
	 * 
	 * @return listReturnmoneys，页面
	 */
	public String selectReturnmoney() {
		listReturnmoneys = returnmoneyService.getObject(returnmoney, "select");
		if (listReturnmoneys != null) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	@Override
	public String getSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDelete() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSelectApp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelectpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDeletepp() {
		// TODO Auto-generated method stub
		return null;
	}
}
