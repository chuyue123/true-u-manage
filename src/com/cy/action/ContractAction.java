package com.cy.action;

import java.util.List;

import com.cy.entity.Contract;
import com.cy.util.IGenerator;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

/***
 * 合同action
 * 
 * @author fzp
 * 
 */
public class ContractAction extends ActionSupport implements
		ModelDriven<Contract>, Action {

	private Contract contract;
	private IGenerator<Contract> contractService;
	private List<Contract> listContracts;

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public IGenerator<Contract> getContractService() {
		return contractService;
	}

	public void setContractService(IGenerator<Contract> contractService) {
		this.contractService = contractService;
	}

	public List<Contract> getListContracts() {
		return listContracts;
	}

	public void setListContracts(List<Contract> listContracts) {
		this.listContracts = listContracts;
	}

	@Override
	public Contract getModel() {
		// TODO Auto-generated method stub
		return contract;
	}

	/**
	 * 添加合同
	 * 
	 * @return 成功页面
	 */
	public String insertContract() {
		boolean flag = contractService.insert(contract, "insert");
		if (flag) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 删除合同
	 * 
	 * @return 成功页面
	 */
	public String deleteContract() {
		boolean flag = contractService.delete(contract, "delete");
		if (flag) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 修改合同
	 * 
	 * @return 修改页面
	 */
	public String updateContract() {
		listContracts = contractService.getObject(contract, "select");
		if (listContracts != null && listContracts.size() == 1) {
			contract = listContracts.get(0);
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 保存修改
	 * 
	 * @return 成功页面
	 */
	public String saveUpdate() {
		boolean flag = contractService.update(contract, "update");
		if (flag) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 查询合同
	 * 
	 * @return listContracts，页面
	 */
	public String selectContract() {
		listContracts = contractService.getObject(contract, "select");
		if (listContracts != null) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	@Override
	public String getSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDelete() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSelectApp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelectpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDeletepp() {
		// TODO Auto-generated method stub
		return null;
	}
}
