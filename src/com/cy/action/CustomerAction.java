package com.cy.action;

import java.util.List;

import net.sf.json.JSONObject;

import com.cy.entity.Customer;
import com.cy.util.IGenerator;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class CustomerAction extends ActionSupport implements
		ModelDriven<Customer>, Action {

	private Customer customer = new Customer();
	private IGenerator<Customer> customerService;
	private List<Customer> listCustomers;
	private JSONObject jo = new JSONObject();

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public IGenerator<Customer> getCustomerService() {
		return customerService;
	}

	public void setCustomerService(IGenerator<Customer> customerService) {
		this.customerService = customerService;
	}

	public List<Customer> getListCustomers() {
		return listCustomers;
	}

	public void setListCustomers(List<Customer> listCustomers) {
		this.listCustomers = listCustomers;
	}

	public JSONObject getJo() {
		return jo;
	}

	public void setJo(JSONObject jo) {
		this.jo = jo;
	}

	@Override
	public Customer getModel() {
		// TODO Auto-generated method stub
		return customer;
	}

	public String insertapp() {
		System.out.println(customer.getName());
		boolean flag = customerService.insert(customer, "Customer.insert");
		if (!flag) {
			jo.put("success", true);
			jo.put("msg", "数据提交失败！！！");
			return "jo";
		}
		jo.put("success", true);
		jo.put("msg", "");
		return "jo";
	}

	public String insertcustomer() {
		String rse = "err";
		if (customer != null) {
			boolean flag = customerService.insert(customer, "customer.insert");
			if (flag) {
				// 加载显示功能

				return SUCCESS;
			} else {
				return rse;
			}
		}
		return rse;

	}

	public String deletecustomer() {
		String rse = "err";
		if (customer != null) {
			boolean flag = customerService.delete(customer, "customer.delete");
			if (flag) {
				// 加载显示功能

				return SUCCESS;
			} else {
				return rse;
			}
		}
		return rse;
	}

	public String updatecustomer() {
		String rse = "err";
		if (customer != null) {
			boolean flag = customerService.update(customer, "customer.update");
			if (flag) {
				// 加载显示功能

				return SUCCESS;
			} else {
				return rse;
			}
		}
		return rse;
	}

	// 条件获取所有信息
	public String getcustomer() {
		String rse = "err";
		if (customer != null) {
			listCustomers = customerService.getObject(customer,
					"customer.insert");
			if (listCustomers != null & listCustomers.size() != 0) {
				// 加载显示功能

				return SUCCESS;
			} else {
				return rse;
			}
		}
		return rse;
	}

	@Override
	public String getSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDelete() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSelectApp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelectpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDeletepp() {
		// TODO Auto-generated method stub
		return null;
	}

}
