package com.cy.action;

import java.util.List;

import org.apache.struts2.ServletActionContext;

import net.sf.json.JSONObject;

import com.cy.entity.CustomerContact;
import com.cy.util.IGenerator;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class CustomerContactAction extends ActionSupport implements
		ModelDriven<CustomerContact>,Action {

	private CustomerContact contact = new CustomerContact();
	private IGenerator<CustomerContact> contactService;
	private List<CustomerContact> listcustomer;
	private JSONObject jo;

	public JSONObject getJo() {
		return this.jo;
	}

	public void setJo(JSONObject jo) {
		this.jo = jo;
	}

	public CustomerContact getContact() {
		return contact;
	}

	public void setContact(CustomerContact contact) {
		this.contact = contact;
	}

	public IGenerator<CustomerContact> getContactService() {
		return contactService;
	}

	public void setContactService(IGenerator<CustomerContact> contactService) {
		this.contactService = contactService;
	}

	public List<CustomerContact> getListcustomer() {
		return listcustomer;
	}

	public void setListcustomer(List<CustomerContact> listcustomer) {
		this.listcustomer = listcustomer;
	}

	@Override
	public CustomerContact getModel() {
		// TODO Auto-generated method stub
		return contact;
	}

	/**
	 * 查询所有
	 */
	@Override
	public String execute() throws Exception {

		ServletActionContext.getResponse().setHeader(
				"Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept");
		ServletActionContext.getResponse().setHeader(
				"Access-Control-Allow-Origin", "*");
		listcustomer = this.contactService.getObject(null, "select");
		if (listcustomer != null) {
			this.jo = new JSONObject();
			this.jo.put("listcustomer", listcustomer);
			return SUCCESS;
		}
		return "err";
	}

	/**
	 * 添加
	 * 
	 * @return
	 */
	public String insert() {
		String res = "err";
		boolean m = this.contactService.insert(contact, "insert");
		if (m) {
			// 根据页面查询是需要的信息
			return "insert";
		}

		return res;
	}

	/**
	 * 删除
	 */
	public String delete() {
		String res = "err";
		boolean m = this.contactService.delete(contact, "delete");
		if (m) {
			// 根据页面查询是需要的信息
			return "delete";
		}
		return res;
	}

	/**
	 * 修改
	 */
	public String update() {
		String res = "err";
		boolean m = this.contactService.update(contact, "update");
		if (m) {
			// 根据页面查询是需要的信息
			return "update";
		}

		return res;
	}

	/**
	 * 查询单个对象所有信息
	 * 
	 * @return
	 */
	public String select() {
		String res = "err";
		contact = this.contactService.getObject(contact, "select").get(0);
		if (contact != null) {

			return "select";
		}
		return res;
	}

	@Override
	public String getSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDelete() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSelectApp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelectpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDeletepp() {
		// TODO Auto-generated method stub
		return null;
	}

}
