package com.cy.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;

import com.cy.entity.User;
import com.cy.util.IGenerator;
import com.cy.util.Util;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class UserAction extends ActionSupport implements ModelDriven<User>, SessionAware, Action {
	private User user = new User();
	private IGenerator<User> userService;
	private List<User> listUsers;
	Util util = new Util();
	private JSONObject jo=new JSONObject();
	private Map<String, Object> session;
	Map map =new HashMap() {{ put( "0" , "管理员" ); 
							  put( "1" , "销售用户" ); 
							  put( "2" , "项目用户" );
							  put( "3" , "考勤用户" );}}; 
		
	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	

	public JSONObject getJo() {
		return jo;
	}

	public void setJo(JSONObject jo) {
		this.jo = jo;
	}

	public List<User> getListUsers() {
		return listUsers;
	}

	public void setListUsers(List<User> listUsers) {
		this.listUsers = listUsers;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public IGenerator<User> getUserService() {
		return userService;
	}

	public void setUserService(IGenerator<User> userService) {
		this.userService = userService;
	}

	@Override
	public User getModel() {
		// TODO Auto-generated method stub
		return user;
	}

	// 默认方法
	@Override
	public String execute() throws Exception {
		// TODO Auto-generated method stub
		return super.execute();
	}

	/**
	 * app登录
	 * 
	 * @return jo，user
	 */
	public String loginapp() {
		ServletActionContext.getResponse().setHeader(
				"Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept");
		ServletActionContext.getResponse().setHeader(
				"Access-Control-Allow-Origin", "*");
		// System.out.println("!!!!!!!!!" + user.getName());
		// System.out.println("!!!!!!!!!" + user);
		listUsers = userService.getObject(user, "User.select");
		if (user.getName() == null || user.getPassword() == null) {
			jo.put("succeess", true);
			jo.put("msg", "用户名密码不能为空！！！");
			return "jo";
		}
		if (listUsers == null || listUsers.size() != 1) {
			jo.put("succeess", true);
			jo.put("msg", "用户名或密码错误！！！");
			return "jo";
		}
		jo.put("succeess", true);
		jo.put("msg", "");
		user = listUsers.get(0);
		jo.put("user", user);
		return "jo";
	}

	/**
	 * app 通过id查询
	 * @return jo,user
	 */
	public String selectId(){
		if(user.getId()==null){
		}
		listUsers=userService.getObject(user, "User.select");
		user=listUsers.get(0);
		jo.put("user", user);
		return "jo";
	}
	/**
	 * 网页登陆
	 * 用户登陆
	 * 
	 * @return
	 */
	public String login() {
		listUsers = userService.getObject(user, "User.select");
		if (user.getName() == null || user.getPassword() == null) {
			return util.setReturn("login", "用户名或密码不能为空！！！");
		}
		if (listUsers.size() == 0 || listUsers == null) {
			return util.setReturn("login", "用户名或密码错误！！！");
		}
		User u = listUsers.get(0);
		session.put("name", u.getName());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		session.put("loginTime", sdf.format(new Date()));
		return util.setReturn(SUCCESS, "登陆成功！！！");
	}

	/**
	 * 用户登出
	 * 
	 * @return
	 */
	public String logout() {
		session.remove("name");
		return util.setReturn("login", "退出成功！！！");
	}

	/**
	 * 查询用户
	 * 
	 * @return listUsers，页面
	 */
	@Override
	public String getSelect() {
		user = new User();
		listUsers = userService.getObject(user, "User.selectList");
		return util.setReturn("user_list", "",map);
	}

	@Override
	public String postSelect() {
		listUsers = userService.getObject(user, "User.select");
		return util.setReturn("user_list", "", map);
	}
	
	/**
	 * 添加用户
	 * 
	 * @return 成功页面
	 */
	@Override
	public String getAdd() {
		return util.setReturn("user_add", "", map);
	}

	@Override
	public String postAdd() {
		if (user.getName().length() == 0 || user.getName() == null || user.getName() == "".trim())
			return util.setReturn("user_add", "用户名不能为空", map);
		if (user.getPassword().length() == 0 || user.getPassword() == null || user.getPassword() == "".trim())
			return util.setReturn("user_add", "密码不能为空", map);
		if (user.getEmail().length() == 0 || user.getEmail() == null || user.getEmail() == "".trim())
			return util.setReturn("user_add", "Email不能为空", map);
		if (user.getTel().length() == 0 || user.getTel() == null || user.getTel() == "".trim())
			return util.setReturn("user_add", "电话号码不能为空", map);
		listUsers = userService.getObject(user, "User.selectCont");
		if (listUsers.size() != 0)
			return util.setReturn("user_add", "用户已存在", map);
		userService.insert(user, "User.insert");
		return getSelect();
	}

	@Override
	public String getUpdate() {
		listUsers = userService.getObject(user, "User.select");
		user = listUsers.get(0);
		return util.setReturn("user_update", "", map);
	}

	@Override
	public String postUpdate() {
		if (user.getName().length() == 0 || user.getName() == null || user.getName() == "".trim())
			return util.setReturn("user_add", "用户名不能为空", map);
		if (user.getPassword().length() == 0 || user.getPassword() == null || user.getPassword() == "".trim())
			return util.setReturn("user_add", "密码不能为空", map);
		if (user.getEmail().length() == 0 || user.getEmail() == null || user.getEmail() == "".trim())
			return util.setReturn("user_add", "Email不能为空", map);
		if (user.getTel().length() == 0 || user.getTel() == null || user.getTel() == "".trim())
			return util.setReturn("user_add", "电话号码不能为空", map);
		listUsers = userService.getObject(user, "User.select");
		if (listUsers == null)
			return util.setReturn("user_update", "用户不存在", map);
		userService.update(user, "User.update");
		return getSelect();
	}

	@Override
	public String getDelete() {
		userService.delete(user, "User.delete");
		return getSelect();
	}

	@Override
	public String getSelectApp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelectpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDeletepp() {
		// TODO Auto-generated method stub
		return null;
	}
}
