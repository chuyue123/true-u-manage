package com.cy.action;

import java.util.List;

import com.cy.entity.Cost;
import com.cy.util.IGenerator;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class CostAction extends ActionSupport implements ModelDriven<Cost>, Action {

	private Cost cost;
	private IGenerator<Cost> costService;
	private List<Cost> listCosts;

	public Cost getCost() {
		return cost;
	}

	public void setCost(Cost cost) {
		this.cost = cost;
	}

	public IGenerator<Cost> getCostService() {
		return costService;
	}

	public void setCostService(IGenerator<Cost> costService) {
		this.costService = costService;
	}

	public List<Cost> getListCosts() {
		return listCosts;
	}

	public void setListCosts(List<Cost> listCosts) {
		this.listCosts = listCosts;
	}

	@Override
	public Cost getModel() {
		// TODO Auto-generated method stub
		return cost;
	}

	/**
	 * 添加花费（需要报销的费用）
	 * 
	 * @return 成功页面
	 */
	public String insertCost() {
		boolean flag = costService.insert(cost, "Cost.insert");
		if (flag) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 删除花费（需要报销的费用）
	 * 
	 * @return 成功页面
	 */
	public String deleteCost() {
		boolean flag = costService.delete(cost, "Cost.delete");
		if (flag) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 修改花费（需要报销的费用）
	 * 
	 * @return 修改页面
	 */
	public String updateCost() {
		listCosts = costService.getObject(cost, "Cost.select");
		if (listCosts != null && listCosts.size() == 1) {
			cost = listCosts.get(0);
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 保存修改
	 * 
	 * @return 成功页面
	 */
	public String saveUpdate() {
		boolean flag = costService.update(cost, "Cost.update");
		if (flag) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 查询花费（需要报销的费用）
	 * 
	 * @return listCosts，页面
	 */
	public String selectCost() {
		listCosts = costService.getObject(cost, "Cost.select");
		if (listCosts != null) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	@Override
	public String getSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDelete() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSelectApp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelectpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDeletepp() {
		// TODO Auto-generated method stub
		return null;
	}
}
