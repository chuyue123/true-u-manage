package com.cy.action;

import java.util.List;

import com.cy.entity.Schedule;
import com.cy.util.IGenerator;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

/**
 * 技术日程action
 * 
 * @author fzp
 * 
 */
public class ScheduleAction extends ActionSupport implements
		ModelDriven<Schedule>, Action {

	private Schedule schedule;
	private IGenerator<Schedule> scheduleService;
	private List<Schedule> listSchedules;

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	public IGenerator<Schedule> getScheduleService() {
		return scheduleService;
	}

	public void setScheduleService(IGenerator<Schedule> scheduleService) {
		this.scheduleService = scheduleService;
	}

	public List<Schedule> getListSchedules() {
		return listSchedules;
	}

	public void setListSchedules(List<Schedule> listSchedules) {
		this.listSchedules = listSchedules;
	}

	@Override
	public Schedule getModel() {
		// TODO Auto-generated method stub
		return schedule;
	}

	@Override
	public String getSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDelete() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSelectApp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelectpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDeletepp() {
		// TODO Auto-generated method stub
		return null;
	}


}
