package com.cy.action;

import java.util.List;

import com.cy.entity.Message;
import com.cy.util.IGenerator;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

/**
 * 信息action
 * 
 * @author fzp
 * 
 */
public class MessageAction extends ActionSupport implements
		ModelDriven<Message>, Action {

	private Message message;
	private IGenerator<Message> messageService;
	private List<Message> listMessages;

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public IGenerator<Message> getMessageService() {
		return messageService;
	}

	public void setMessageService(IGenerator<Message> messageService) {
		this.messageService = messageService;
	}

	public List<Message> getListMessages() {
		return listMessages;
	}

	public void setListMessages(List<Message> listMessages) {
		this.listMessages = listMessages;
	}

	@Override
	public Message getModel() {
		// TODO Auto-generated method stub
		return message;
	}

	/**
	 * 添加信息
	 * 
	 * @return 成功页面
	 */
	public String insertMessage() {
		boolean flag = messageService.insert(message, "insert");
		if (flag) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 删除信息
	 * 
	 * @return 成功页面
	 */
	public String deleteMessage() {
		boolean flag = messageService.delete(message, "delete");
		if (flag) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 修改信息
	 * 
	 * @return 修改页面
	 */
	public String updateMessage() {
		listMessages = messageService.getObject(message, "select");
		if (listMessages != null && listMessages.size() == 1) {
			message = listMessages.get(0);
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 保存修改
	 * 
	 * @return 成功页面
	 */
	public String saveUpdate() {
		boolean flag = messageService.update(message, "update");
		if (flag) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	/**
	 * 查询信息
	 * 
	 * @return listMessages，页面
	 */
	public String selectMessage() {
		listMessages = messageService.getObject(message, "select");
		if (listMessages != null) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	@Override
	public String getSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDelete() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSelectApp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postSelectpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postAddpp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String postUpdatepp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDeletepp() {
		// TODO Auto-generated method stub
		return null;
	}
}
