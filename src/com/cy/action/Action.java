package com.cy.action;

/**
 * 实现增删改查方法
 * @author Administrator
 *
 */
public interface Action {
	public String getSelect();
	public String postSelect();
	public String getAdd();
	public String postAdd();
	public String getUpdate();
	public String postUpdate();
	public String getDelete();
	public String getSelectApp();
	public String postSelectpp();
	public String getAddpp();
	public String postAddpp();
	public String getUpdatepp();
	public String postUpdatepp();
	public String getDeletepp();
}
